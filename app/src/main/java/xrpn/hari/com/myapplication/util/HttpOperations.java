package xrpn.hari.com.myapplication.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import xrpn.hari.com.myapplication.MainActivity;
import xrpn.hari.com.myapplication.R;

/**
 * Created by root on 16/1/18.
 */
    public class HttpOperations {

    String binance_url="https://api.binance.com/api/v3/ticker/price?symbol=XRPETH";
//    String koinex_url="https://koinex.in/api/ticker";
    String koinex_url="https://koinex.in/api/dashboards/ticker";

    public void getData(final Context mContext) {

        final RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        JsonObjectRequest jsab=new JsonObjectRequest(Request.Method.GET, binance_url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject bin_response) {
                final JSONObject binnresp=bin_response;
                Log.d(">>>>>>>>>>>> binace ",bin_response.toString());

                JsonObjectRequest jkoin=new JsonObjectRequest(Request.Method.GET, koinex_url, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(">>>>>>>>>>>> koinex ",response.toString());


                        try {
                            JSONObject jprice=response.getJSONObject("prices");
                            float bin_price= Float.parseFloat( binnresp.getString("price"));
                            float xrp_price= Float.parseFloat( response.getString("XRP"));
                            float eth_price= Float.parseFloat( response.getString("ETH"));

                            float val=((1/bin_price)*xrp_price)-eth_price;
                            Log.d("differnce >> ", "onResponse: "+val);

                            SharedPreferences pref = mContext.getSharedPreferences("pref_coin_101", 0);

                            int diff=pref.getInt("diff",0);

                            SharedPreferences.Editor edt=pref.edit();



                            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, new Intent(mContext, MainActivity.class), 0);

                            Calendar c = Calendar.getInstance();
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String formattedDate = df.format(c.getTime());
                            String msg="Current Differnce = "+val +"\n XRP="+xrp_price+" \n ETH = "+eth_price +"\n time ="+formattedDate;
                            edt.putString("last_val",msg);
                            edt.commit();
                            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                            Notification notification = new NotificationCompat.Builder(mContext)
                                    .setContentTitle("Differnce ")
                                    .setContentText( "Current Differnce = "+val +" XRP="+xrp_price+" ETH = "+eth_price +" time ="+formattedDate)

                                    .setAutoCancel(true)
                                    .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                                    .setSound(soundUri)
                                    .setContentIntent(pendingIntent)
                                     .setSmallIcon(R.mipmap.ic_launcher)
                                   // .setShowWhen(true)
                                    .setColor(Color.GREEN)
                                   // .setLocalOnly(true)
                                    .build();

                           // NotificationManagerCompat.from(mContext).notify(101, notification);
                            NotificationManager mNotifyMgr = (NotificationManager)mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                            Log.d(">>>>>", "onResponse: sending notification");
                            if(val >= diff || val <= (-diff))
                            mNotifyMgr.notify(101, notification);

                        }catch (Exception exp){
                            Log.d(">>>>>", "onResponse: "+exp.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(">>>>", "onErrorResponse: " +error.getMessage());
                    }
                });
                requestQueue.add(jkoin);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(">>>>>>> binace error ",error.getMessage());
            }
        });


        requestQueue.add(jsab);
    }
}
