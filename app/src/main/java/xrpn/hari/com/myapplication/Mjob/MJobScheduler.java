package xrpn.hari.com.myapplication.Mjob;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import xrpn.hari.com.myapplication.MainActivity;
import xrpn.hari.com.myapplication.R;

/**
 * Created by root on 18/1/18.
 */
public class MJobScheduler extends JobService {
    private MjobExecutor mjobExecutor;
    @Override
    public boolean onStartJob(final JobParameters params) {
        mjobExecutor=new MjobExecutor(getApplicationContext()){
            @Override
            protected void onPostExecute(String s) {
//                super.onPostExecute(s);


                Log.d(">>>>>>", "onPostExecute: ");
                Toast.makeText(getApplicationContext()," result "+s,Toast.LENGTH_SHORT).show();
                if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    scheduleRefresh();
                jobFinished(params,false);
            }
        };
        mjobExecutor.execute();

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        mjobExecutor.cancel(true);
        return false;
    }

    private void scheduleRefresh() {
        JobScheduler mJobScheduler = (JobScheduler)getApplicationContext()
                .getSystemService(JOB_SCHEDULER_SERVICE);
        JobInfo.Builder mJobBuilder =
                new JobInfo.Builder(101,
                        new ComponentName(getPackageName(),
                                MJobScheduler.class.getName()));

  /* For Android N and Upper Versions */
       SharedPreferences pref = getApplicationContext().getSharedPreferences("pref_coin_101", 0);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mJobBuilder
                    .setMinimumLatency(pref.getLong("mills",(10*60)*1000)) //YOUR_TIME_INTERVAL
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        }


        if (mJobScheduler != null && mJobScheduler.schedule(mJobBuilder.build())
                <= JobScheduler.RESULT_FAILURE) {
            //Scheduled Failed/LOG or run fail safe measures
            Log.d(">>>", "Unable to schedule the service!");
        }
    }
}
